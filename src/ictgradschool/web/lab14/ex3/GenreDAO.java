package ictgradschool.web.lab14.ex3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class GenreDAO {
    public void searchByGenre() {

        System.out.println("\n\nPlease enter the name of the genre you wish to get information about, or press enter to return to the previous menu \n");
        String genre = Keyboard.readInput();
        if (genre.length()==0){
            return;
        }

        try (Connection conn = ConnectionManager.getConnection()) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM lab14_films WHERE genre LIKE ?")) {
                stmt.setString(1, "%" + genre + "%");
                try (ResultSet rs = stmt.executeQuery()) {


                    if (!rs.isBeforeFirst()){
                        System.out.println("\nSorry, we couldn't find any films by that genre\n");
                        return;
                    }

                    System.out.println("\nThe " + genre + " genre includes the following films:\n");
                    while (rs.next()) {
                        System.out.println(rs.getString(rs.findColumn("name")));
                    }
                    System.out.println();

                }
            }

        } catch (SQLException e) {
            System.out.println("Failed to create the database connection.");
        }
    }
}
