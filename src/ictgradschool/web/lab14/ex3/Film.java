package ictgradschool.web.lab14.ex3;

/**
 * Created by chw4 on 27/04/2017.
 */
public class Film {
    private int filmID;
    private String name;
    private String genre;

    public void setFilmID(int id){
        this.filmID = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setGenre(String genre){
        this.genre = genre;
    }

    public int getFilmID(){
        return filmID;
    }

    public String getName(){
        return name;
    }

    public String getGenre(){
        return genre;
    }

}
