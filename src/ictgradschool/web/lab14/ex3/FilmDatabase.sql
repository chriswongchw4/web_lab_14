DROP TABLE IF EXISTS lab14_involve;
DROP TABLE IF EXISTS lab14_films;
DROP TABLE IF EXISTS lab14_genre;
DROP TABLE IF EXISTS lab14_actors;


CREATE TABLE lab14_actors (
    actorid int(3) NOT NULL,
    name VARCHAR(20) NOT NULL,
    PRIMARY KEY (actorid)
);

CREATE TABLE lab14_genre (
    genre VARCHAR(20) NOT NULL,
    PRIMARY KEY (genre)
);

CREATE TABLE lab14_films (
    filmid int(3) NOT NULL,
    name VARCHAR(32) NOT NULL,
    genre VARCHAR(20) NOT NULL,
    PRIMARY KEY (filmid),
    FOREIGN KEY (genre) REFERENCES lab14_genre(genre)
);

CREATE TABLE lab14_involve (
    filmid int(3) NOT NULL,
    actorid int(3) NOT NULL,
    role VARCHAR(10) NOT NULL,
    PRIMARY KEY (filmid,actorid),
    FOREIGN KEY (filmid) REFERENCES lab14_films(filmid),
    FOREIGN KEY (actorid) REFERENCES lab14_actors(actorid)
);

INSERT INTO lab14_actors VALUES
    (1,'Keanu Reeves'),
    (2,'Jason Sudeikis'),
    (3,'Peter Dinklage'),
    (4,'Clay Kaytis');

INSERT INTO lab14_genre VALUES
    ('Surreal'),
    ('test');

INSERT INTO lab14_films VALUES
    (1,'The Matrix','test'),
    (2,'John Wick','test'),
    (3,'Angry Birds','test'),
    (4,'Rubber','Surreal'),
    (5,'Alice','Surreal');

INSERT INTO lab14_involve VALUES
    (1,1,'Lead'),
    (2,1,'Lead'),
    (3,2,'Lead'),
    (3,3,'Eagle'),
    (3,4,'Director');

