package ictgradschool.web.lab14.ex3;

/**
 * Created by chw4 on 27/04/2017.
 */
public class Genre {
    private String genre;

    public void setGenre(String genre){
        this.genre = genre;
    }

    public String getGenre(){
        return genre;
    }
}
