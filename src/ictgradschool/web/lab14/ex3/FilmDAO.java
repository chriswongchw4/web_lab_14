package ictgradschool.web.lab14.ex3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by chw4 on 27/04/2017.
 */
public class FilmDAO {
    public void searchByActor(){

        System.out.println("\n\nPlease enter the name of the actor you wish to get information about, or press enter to return to the previous menu\n");
        String actorName = Keyboard.readInput();
        if (actorName.length()==0){
            return;
        }

        try (Connection conn = ConnectionManager.getConnection()) {
            try (PreparedStatement stmt = conn.prepareStatement(
                    "SELECT * FROM lab14_films,lab14_actors,lab14_involve WHERE lab14_actors.actorid=lab14_involve.actorid AND lab14_films.filmid=lab14_involve.filmid AND lab14_actors.name LIKE ?"
            )) {
                stmt.setString(1, "%" + actorName + "%");

                try (ResultSet rs = stmt.executeQuery()) {

                    if (!rs.isBeforeFirst()){
                        System.out.println("\nSorry, we couldn't find any films by that name.\n");
                        return;
                    }
                    System.out.println("\n" + rs.findColumn("lab14_actors.name") + " is listed as being involved in the following films: \n");
                    while (rs.next()) {
                        System.out.println(rs.getString(rs.findColumn("lab14_films.name")) + " (" + rs.getString(rs.findColumn("lab14_involve.role"))+ ")");
                    }
                    System.out.println();
                }

            }
        } catch (SQLException e) {
            System.out.println("Failed to create the database connection.");
        }
    }
}
