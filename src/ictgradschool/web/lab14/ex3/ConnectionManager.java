package ictgradschool.web.lab14.ex3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class ConnectionManager {

    private static String url = "jdbc:mysql://mysql1.sporadic.co.nz:3306/chw4";
    private static String driverName = "com.mysql.jdbc.Driver";
    private static Connection con;


    public static Connection getConnection(){
        try {
            Class.forName(driverName);
            try {
                Properties properties = new Properties();
                properties.setProperty("user", "chw4");
                properties.setProperty("password", "10938592");
                properties.setProperty("useSSL", "true");
                con = DriverManager.getConnection(url, properties);
            } catch (SQLException ex) {
                // log an exception. for example:
                System.out.println("Failed to create the database connection.");
            }
        } catch (ClassNotFoundException ex) {
            // log an exception. for example:
            System.out.println("Driver not found.");
        }
        return con;
    }
}
