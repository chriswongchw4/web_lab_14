package ictgradschool.web.lab14.ex3;

public class Exercise03 {
    public static void main(String[] args) {
        Exercise03 ex03 = new Exercise03();
        ex03.start();
    }


    public void start() {

        printWelcomeMessage();

        boolean exit = false;

        while (exit == false) {
            printMenu();
            int input = getUserInput();
            if (input == 4) {
                exit = true;
            }
            parseCommand(input);
        }
    }


    private int getUserInput() {
        int option = 0;
        while (option == 0) {
            try {
                option = Integer.parseInt(Keyboard.readInput());
                if (!(option >= 1 && option <= 4)) {
                    System.out.println("Please select the number between 1-4.");
                }else if (option >= 1 && 4 <= option){
                    return option;
                }
            } catch (NullPointerException e) {
                System.out.println("Please enter a number.");
            } catch (NumberFormatException e) {
                System.out.println("Please enter a number.");
            }
        }
        return option;
    }

    private void parseCommand(int option) {
        int command = option;

        switch (command) {
            case 1:
                FilmDAO film = new FilmDAO();
                film.searchByActor();
                break;
            case 2:
                ActorDAO actor = new ActorDAO();
                actor.searchMovie();
                break;
            case 3:
                GenreDAO genre = new GenreDAO();
                genre.searchByGenre();
                break;
        }

    }

    private static void printMenu() {
        System.out.println("Please select an option from the following: ");
        System.out.println("1. Information by Actor");
        System.out.println("2. Information by Movie");
        System.out.println("3. Information by Genre");
        System.out.println("4. Exit");
        System.out.println();
        System.out.print(">");

    }

    private void printWelcomeMessage() {
        System.out.println("Welcome to the Film database! \n");
    }

}
