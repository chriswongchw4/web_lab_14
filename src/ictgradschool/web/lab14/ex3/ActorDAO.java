package ictgradschool.web.lab14.ex3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ActorDAO {

    public void searchMovie() {

        System.out.println("\n\nPlease enter the name of the film you wish to get information about, or press enter to return to the previous menu\n");
        String movieName = Keyboard.readInput();
        if (movieName.length()==0){
            return;
        }
        System.out.println();

        try (Connection conn = ConnectionManager.getConnection()) {
            try (PreparedStatement stmt = conn.prepareStatement(
                    "SELECT * FROM lab14_actors,lab14_films,lab14_involve WHERE lab14_films.name LIKE ? AND lab14_actors.actorid=lab14_involve.actorid AND lab14_films.filmid=lab14_involve.filmid"
            )) {
                stmt.setString(1, "%" + movieName + "%");

                try (ResultSet rs = stmt.executeQuery()) {

                    if (!rs.isBeforeFirst()){
                        System.out.println("\nSorry, we couldn't find any films by that name.\n");
                        return;
                    }
                    while (rs.next()) {
                        System.out.println(rs.getString(rs.findColumn("lab14_actors.name")) + " ("+ rs.getString(rs.findColumn("lab14_involve.role"))+ ")");
                    }
                    System.out.println();
                }
            }
        } catch (SQLException e) {
            System.out.println("Failed to create the database connection.");
        }
    }

}
