package ictgradschool.web.lab14.ex1;

import java.io.IOException;
import java.sql.*;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Configure the following with your details
        Properties properties = new Properties();
        properties.setProperty("user", "chw4");
        properties.setProperty("password", "10938592");
        properties.setProperty("useSSL", "true");

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/chw4", properties)) {

            String articlebody = "";

            while (articlebody == ""){
                try (PreparedStatement stmt = conn.prepareStatement(
                        "SELECT * FROM simpledao_articles WHERE title LIKE ?;"
                )){
                    System.out.print("Please enter the article title: ");
                    String title = Keyboard.readInput();
                    stmt.setString(1, "%"+ title + "%");

                    try (ResultSet r = stmt.executeQuery()){
                        while (r.next()){
                            articlebody = r.getString(r.findColumn("body"));

                            String articlename = r.getString(r.findColumn("title"));
                            System.out.println("Here is the article for title - " + articlename + ": \n" + articlebody);
                        }
                        if (articlebody==""){
                            System.out.println("No such article.");
                        }

                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }






    public static class Keyboard {

        private static Scanner in = new Scanner(System.in);
        private static boolean redirected = false;

        public static String readInput() {

            try {
                if (!redirected) {
                    redirected = System.in.available() != 0;
                }
            } catch (IOException e) {
                System.err.println("An error has occurred in the Keyboard constructor.");
                e.printStackTrace();
                System.exit(-1);
            }

            try {
                String input = in.nextLine();
                if (redirected) {
                    System.out.println(input);
                }
                return input;
            } catch (NoSuchElementException e) {
                return null; // End of file
            } catch (IllegalStateException e) {
                System.err.println("An error has occurred in the Keyboard.readInput() method.");
                e.printStackTrace();
                System.exit(-1);
            }
            return null;
        }
    }
}
