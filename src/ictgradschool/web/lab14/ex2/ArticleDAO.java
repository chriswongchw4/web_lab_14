package ictgradschool.web.lab14.ex2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by chw4 on 27/04/2017.
 */
public class ArticleDAO {

    public List<Article> allArticle() {

        List<Article> articles = new ArrayList<>();

        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Configure the following with your details
        Properties properties = new Properties();
        properties.setProperty("user", "chw4");
        properties.setProperty("password", "10938592");
        properties.setProperty("useSSL", "true");

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/chw4", properties)) {

            try (Statement stmt = conn.createStatement()) {
                try (ResultSet r = stmt.executeQuery("SELECT * FROM simpledao_articles")) {
                    while (r.next()) {
                        Article a = new Article();
                        a.setArticleID(r.getInt(1));
                        a.setArticleTitle(r.getString(2));
                        a.setArticleBody(r.getString(3));
                        articles.add(a);
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < articles.size(); i++) {
            System.out.println("Article Number: " + i);
            System.out.println(articles.get(i).toString());
        }
        return articles;
    }
}
