package ictgradschool.web.lab14.ex2;

/**
 * Created by chw4 on 27/04/2017.
 */
public class Article {
    private int articleID;
    private String articleTitle;
    private String articleBody;

    public void setArticleID(int id) {
        this.articleID = id;
    }

    public void setArticleTitle(String title) {
        this.articleTitle = title;
    }

    public void setArticleBody(String body) {
        this.articleBody = body;
    }

    public int getArticleID() {
        return articleID;
    }

    public String getArticleTitle(){
        return articleTitle;
    }

    public String getArticleBody(){
        return articleBody;
    }

    public String toString(){
        String article = articleTitle + "\n" + articleBody;
        return article;
    }

}
